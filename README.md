# `xpub` Deployment Config

This repository contains a base image for [xpub](https://gitlab.coko.foundation/xpub/xpub) and the templates used to deploy to Kubernetes.

## Templates

See [pubsweet/infra](https://gitlab.coko.foundation/pubsweet/infra) and [pubsweet/deployer](https://gitlab.coko.foundation/pubsweet/deployer) for more details about how these templates work.

## Images

The base image for xpub creates a yarn offline mirror from the current `xpub` repository in order to speed up builds there. It can be rebuilt and pushed to docker hub by triggering the relevant job in this repository. The image for `xpub` itself extends this base image.

